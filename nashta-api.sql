CREATE TABLE `mahasiswa` (
  `id` integer PRIMARY KEY,
  `nama` varchar(225),
  `alamat` varchar(555)
);

CREATE TABLE `mata_kuliah` (
  `id` integer PRIMARY KEY,
  `nama_mata_kuliah` varchar(225),
  `mahasiswa_id` integer
);

CREATE TABLE `data_nilai` (
  `id` integer PRIMARY KEY,
  `mahasiswa_id` integer,
  `mata_kuliah_id` integer,
  `nilai` integer,
  `keterangan` varchar(1000)
);

ALTER TABLE `data_nilai` ADD FOREIGN KEY (`mahasiswa_id`) REFERENCES `mahasiswa` (`id`);

ALTER TABLE `data_nilai` ADD FOREIGN KEY (`mata_kuliah_id`) REFERENCES `mata_kuliah` (`id`);

ALTER TABLE `mata_kuliah` ADD FOREIGN KEY (`mahasiswa_id`) REFERENCES `mahasiswa` (`id`);
