const express = require('express');
const dbConnection = require('../../connections/dbConnection');
const app = express();


app.get('/data_nilai/:id',async(req,res)=>{
    const mahasiswa_id = req.params.id
    const rerataNilai = await dbConnection('data_nilai').avg('nilai').where({id:mahasiswa_id})
    res.send(rerataNilai)
})

module.exports = app