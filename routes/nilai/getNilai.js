const express = require('express');
// const data_nilai = require('../../databases/dataNilai');
const dbConnection = require('../../connections/dbConnection');
const app = express();

app.get('/data_nilai',async(req,res)=>{
    const body = req.body
    const nilai = await dbConnection('data_nilai').where(body)
    res.send(nilai)
})

module.exports = app