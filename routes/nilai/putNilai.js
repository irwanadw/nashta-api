const express = require('express');
const dbConnection = require('../../connections/dbConnection');
const app = express();

app.put('/data_nilai', async(req,res)=>{
    // const mahasiswa_id = req.query.id
    const body = req.body
    await dbConnection('data_nilai').where(body)
    res.send(body)
})

module.exports = app