const express = require('express');
const dbConnection = require('../../connections/dbConnection');
const app = express();

app.post('/data_nilai/:id', async(req,res)=>{
    const mahasiswa_id = req.params.id
    const body = req.body
    await dbConnection('data_nilai').where({id: mahasiswa_id}).insert(body,['id','mata_kuliah_id','nilai', 'keterangan' ])
    res.send(req.body)
})

module.exports = app