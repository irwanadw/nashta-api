const express = require('express');
const dbConnection = require('../../connections/dbConnection');
const app = express();

app.delete(`/data_nilai/`, async (req,res)=>{
    const body = req.body
    await dbConnection('data_nilai').where(body).del()
    res.send("delete berhasil")
})

module.exports = app