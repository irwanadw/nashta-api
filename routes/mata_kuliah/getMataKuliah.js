const express = require('express');
const dbConnection = require('../../connections/dbConnection');
const app = express();

app.get('/mata_kuliah', async(req,res)=>{
    const body = req.body
    const mataKuliah = await dbConnection('mata_kuliah').where(body)
    res.send(mataKuliah)
})

module.exports = app