const express = require('express');
const dbConnection = require('../../connections/dbConnection');
const app = express();

app.post('/mahasiswa',async (req,res)=>{
    const body = req.body
    await dbConnection('mahasiswa').insert(body)
    res.send(body)
})

module.exports = app