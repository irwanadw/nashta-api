const express = require('express');
const dbConnection = require('../../connections/dbConnection');
const app = express();

app.get('/mahasiswa', async(req,res)=>{
    const body = req.body
    const mahasiswa = await dbConnection('mahasiswa').where(body)
    res.send(mahasiswa)
})

module.exports = app