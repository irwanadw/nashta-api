const express = require('express');
const dbConnection = require('../../connections/dbConnection');
const mahasiswa = require('../../databases/dataMahasiswa');
const app = express();

app.get('/mahasiswa/:id', async(req,res)=>{
    const body = req.body
    const id = req.params.id
    const mahasiswaById = await dbConnection('mahasiswa').where({id:id})
    res.send(mahasiswaById)
})

module.exports = app