const express = require('express');
const app = express();
app.use(express.json());

const rootRoute = require('./routes/rootRoute');
const getNilai = require('./routes/nilai/getNilai');
const getRerataNilai = require('./routes/nilai/getRerataNilaiById');
const addNilai = require('./routes/nilai/addNilai');
const deleteNilai = require('./routes/nilai/deleteNilai');
const putNilai = require('./routes/nilai/putNilai');
const getMahasiswa = require('./routes/mahasiswa/getMahasiswa');
const getMahasiswaById = require('./routes/mahasiswa/getMahasiswaById');
const addMahasiswa = require('./routes/mahasiswa/addMahasiswa');
const getMataKuliah = require('./routes/mata_kuliah/getMataKuliah');

app.use(rootRoute);
app.use(getNilai);
app.use(getRerataNilai);
app.use(addNilai);
app.use(deleteNilai);
app.use(putNilai);
app.use(getMahasiswa);
app.use(getMahasiswaById);
app.use(addMahasiswa);
app.use(getMataKuliah);

const PORT = 3000
app.listen(PORT, ()=>{
    console.log(`Server running on http://localhost:${PORT}`);
})